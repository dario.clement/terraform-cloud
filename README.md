# Terraform cloud

Terraform Cloud is an application that helps teams use Terraform together. It manages Terraform runs in a consistent and reliable environment, and includes easy access to shared state and secret data, access controls for approving changes to infrastructure

## Workspaces

Terraform Cloud manages infrastructure collections with `workspaces` instead of directories. A workspace contains everything Terraform needs to manage a given collection of infrastructure, and separate workspaces function like completely separate working directories.

[Workspace doc](https://www.terraform.io/cloud-docs/workspaces)

## Variable and secrets

Terraform Cloud requires cloud provider access token, these are kept as sensitive variables and configurable via the Terraform resource [tfe_variable](https://registry.terraform.io/providers/hashicorp/tfe/latest/docs/resources/variable).

[Variables doc](https://www.terraform.io/cloud-docs/workspaces/variables)

### Encryption

Because we can manage Terraform Cloud from Terraform, we can also use `sops` to manage required secrets.

Using `sops` to encrypt AWS credentials.

```
> AWS_PROFILE=demo sops secrets.enc.tfvars
```

## Workflow

With version control workflow using Gitlab, commits and merge requests will notify Terraform Cloud to run terraform plans in affected workspaces. Only VCS commit runs will persist in Terraform Cloud workspaces, MR runs will not diplay in the workspace run history.

Once a workspace is connected to a repository, each commit to the specific branch will trigger an external step in the Gitlab pipeline, this step will generate the link to the Terraform Cloud run and will fail if the terraform plan fails to complet.

![gitlab pipeline](./images/gitlab_pipeline.png)

Following the external pipeline step bring us to the run page:

![tfc planned](./images/tfc_planned.png)

Upon successful terraform plan, we can apply the changes from the same page:

![tfc applied](./images/tfc_applied.png)

If terraform plan failed for whatever reason, the pipeline step in gitlab will fail and the external link we display the error message:

![tfc error](./images/tfc_error.png)

Gitlab successful plan:

![gitlab passed](./images/gitlab-pipeline-passed.png)

Gitlab failed plan:

![gitlab failed](./images/gitlab-pipeline-failed.png)

Multiple commits without clearing previous plans will cause Terraform Cloud to put subsequent run in pending, these runs will not be displayed in Gitlab pipelines untill previous plans are applied or discarded.

![tfc pending](./images/tfc_pending_plan.png)

## VCS integration

Connecting Terraform Cloud to your VCS involves four steps:

| Step | On your VCS                                                                    | On Terraform Cloud                                                          |
|------|--------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| 1    |                                                                                | Create a new connection in Terraform Cloud. Get redirect URI.               |
| 2    | Register your Terraform Cloud organization as a new app. Provide redirect URI. |                                                                             |
| 3    |                                                                                | Provide Terraform Cloud with application ID and secret. Request VCS access. |
| 4    | Approve access request.                                                        |                                                                             |

Generate an access token with API scope and set it to `oauth_token` terraform variable.
This token is used globally for every Terraform cloud `workspace`.
Each workspace requires an identifier to link to the repository pipeline (ex: BuildingRobotics/pipelines).

[Gitlab integration doc](https://www.terraform.io/cloud-docs/vcs/gitlab-com)

## Notifications

Run status can be forwarded using Slack when provided an application webhook URL, notification control can be applied to only forward certain events (ex: failed plan, action required).

[Notifications doc](https://www.terraform.io/cloud-docs/workspaces/settings/notifications)

## Single sign on

https://www.terraform.io/cloud-docs/users-teams-organizations/single-sign-on

## Workspace structure

https://www.terraform.io/cloud-docs/guides/recommended-practices/part1#the-recommended-terraform-workspace-structure

One Workspace Per Environment Per Terraform Configuration:

```
aws-comfy-dev
aws-comfy-stage
aws-comfy-prod
aws-root-dev
aws-root-stage
aws-root-prod
```

## Local execution with VCS workspaces

After setting up VCS workspaces in Terraform cloud you can still run `plan` locally and view remote state with `show` but `apply` will be blocked for local execution.

You can still `show` the remote state from the workspace:
```
❯ terraform show                                                                                                           
# aws_instance.web:                                                                                                        
resource "aws_instance" "web" {                                                                                            
    ami                                  = "ami-0cb4e786f15603b0d"         
    arn                                  = "arn:aws:ec2:us-west-2:982351172607:instance/i-0576c02cd2cee8d4b"               
    associate_public_ip_address          = true              
    availability_zone                    = "us-west-2d"
```

Running `plan` will now generate a Terraform cloud link to share the plan results.
```
❯ terraform plan                                                                                                           
Running plan in the remote backend. Output will stream here. Pressing Ctrl-C                                               
will stop streaming the logs, but will not stop the plan running remotely.                                                 

Preparing the remote plan...                                                                                               

The remote workspace is configured to work with configuration at                                                           
workspace1 relative to the target repository.                                                                              

To view this run in a browser, visit:                        
https://app.terraform.io/app/tf_managed_org/tf_managed_workspace1/runs/run-k8zdhriiY8Uh3vAp
```

Running terraform apply locally after having setup VCS workspace will result in an error:
```
❯ terraform apply
│ Error: Apply not allowed for workspaces with a VCS connection│
│ A workspace that is connected to a VCS requires the VCS-driven workflow to ensure that the VCS remains the single source of truth.
```

## Managing Terraform cloud

[Terraform Cloud provider](https://registry.terraform.io/providers/hashicorp/tfe/0.31.0)

We can manage Terraform Cloud configurations with Terraform itself, the provider gives us resources to create and manage workspaces.

This demo was created with the folder structure below, it contains 2 functional workspaces and the Terraform Cloud managed resource folder.

```
├── tf_cloud
│   ├── main.tf
│   ├── notifications.tf
│   ├── variable_set.tf
│   ├── variables.tf
│   ├── versions.tf
│   └── workspaces.tf
├── workspace1
│   ├── main.tf
│   └── versions.tf
└── workspace2
    ├── main.tf
    └── versions.tf
```

Managing the Terraform Cloud provider state inside Terraform Cloud itself caused a few issues, this state was kept on S3.

## Spike Acceptance Criteria

[] Figure out how to setup multiple projects for each environment. (Each env can have it’s own Loki and Tor databases)

Terraform Cloud manages infrastructure collections with workspaces, each workspace holds the remote state for the resources it manages, a single git repository can have multiple workspaces when defining different working directories.

[] How to create a functional IAM account for granting access to Terraform cloud

An IAM user similar to gitlab user which has permissions to CD policies.
Generate access key and update `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` for Terraform cloud to use.
These can be kept in Terraform cloud as sensitive variable sets.

```
resource "tfe_variable" "AWS_SECRET_ACCESS_KEY" {
  key             = "AWS_SECRET_ACCESS_KEY"
  value           = var.AWS_SECRET_ACCESS_KEY
  category        = "env"
  sensitive       = true
  variable_set_id = tfe_variable_set.default.id
}
```

[] Figure out how to integrate tf state files and secrets to cloud

At a minimum Terraform cloud needs to manage the infrastructure access tokens, we can still use `sops` to manage the application and cluster secrets.

[] Figure out integrate GitLab job in IAC with TF cloud

We need to generate an access token with API scope and set it to `oauth_token` terraform variable.
This token is used globally for every Terraform cloud `workspace`.
Each workspace requires an identifier to link to the repository pipeline (ex: BuildingRobotics/pipelines).

[] Document all findings in this ticket

 - concurrent plans will pause until current plans are applied or discarded.
 - merge requests will trigger Terraform Cloud to run `plan` and a link the results in the pipeline external section, these runs are not kept in history and cannot be applied.
 - need to figure out RBAC.
 - terraform plan can still be run locally, results will stream to both the local console and Terraform Cloud.
 -

[] Present recommedations on how to move forward to the team

If the workflow is accepted by the team, we could work towards a working proof of concept with the iac repo.

[] Stretch goal: Proof-of-concept in our newly created TF cloud account
