provider "tfe" {
  token    = var.tfe_token
}

#data "tfe_organization_membership" "default" {
#  organization = var.organization
#  email        = var.user_email
#}

// Create an organization
resource "tfe_organization" "default" {
  name  = var.organization
  email = var.user_email
}

// Authenticate with VCS
resource "tfe_oauth_client" "default" {
  organization = tfe_organization.default.name
  api_url = "https://gitlab.com/api/v4/"
  http_url = "https://gitlab.com"
  service_provider = "gitlab_hosted"
  oauth_token = var.oauth_token
}

