// Slack integration for terraform 1 workspace
resource "tfe_notification_configuration" "default" {
  count            = 0 // disable this for now
  name             = "slack-notification-configuration"
  enabled          = false
  destination_type = "slack"
  triggers         = ["run:created", "run:planning", "run:errored"]
  workspace_id     = tfe_workspace.one.id
  token            = ""
  url              = ""
}

