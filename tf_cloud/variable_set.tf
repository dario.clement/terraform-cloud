resource "tfe_variable_set" "default" {
  name          = "AWS Varset"
  description   = "AWS access variables"
  organization  = tfe_organization.default.name
  workspace_ids = [tfe_workspace.one.id, tfe_workspace.two.id]
}

resource "tfe_variable" "AWS_ACCESS_KEY_ID" {
  key             = "AWS_ACCESS_KEY_ID"
  value           = var.AWS_ACCESS_KEY_ID
  category        = "env"
  sensitive       = true
  variable_set_id = tfe_variable_set.default.id
}

resource "tfe_variable" "AWS_SECRET_ACCESS_KEY" {
  key             = "AWS_SECRET_ACCESS_KEY"
  value           = var.AWS_SECRET_ACCESS_KEY
  category        = "env"
  sensitive       = true
  variable_set_id = tfe_variable_set.default.id
}

resource "tfe_variable" "AWS_REGION" {
  key             = "AWS_REGION"
  value           = var.AWS_REGION
  category        = "env"
  sensitive       = true
  variable_set_id = tfe_variable_set.default.id
}
