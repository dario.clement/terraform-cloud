variable "branch" {
  description = "The repository branch that Terraform will execute from."
  default     = "main"
  type        = string
}

variable "identifier" {
  description = "A reference to your VCS repository in the format :org/:repo where :org and :repo refer to the organization and repository in your VCS provider."
  type        = string
}

variable "organization" {
  default     = "tf_managed_org"
  description = "Name of the organization."
  type        = string
}

variable "user_email" {
  default     = "cloud@comfyapp.com"
  description = "User email"
  type        = string
}

variable "tfe_token" {
  description = "The token used to authenticate with Terraform Enterprise. Only set this argument when running in a Terraform Enterprise workspace; for CLI usage, omit the token from the configuration and set it as `credentials` in the CLI config file or set the `TFE_TOKEN` environment variable."
  default     = null
  type        = string
}

variable "oauth_token" {
  default     = null
  description = "Token of the VCS Connection (OAuth Connection Token) to use."
  type        = string
}

variable "AWS_ACCESS_KEY_ID" {
  type        = string
}

variable "AWS_SECRET_ACCESS_KEY" {
  type        = string
}

variable "AWS_REGION" {
  type        = string
}
