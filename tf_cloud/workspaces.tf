resource "tfe_workspace" "one" {
  name              = "tf_managed_workspace1"
  organization      = tfe_organization.default.name
  working_directory = "workspace1"
  description       = "Terraform managed workspace"
  tag_names         = ["test", "app", "1"]

  vcs_repo {
    branch             = var.branch
    identifier         = var.identifier
    oauth_token_id     = tfe_oauth_client.default.oauth_token_id
  }
}

resource "tfe_workspace" "two" {
  name              = "tf_managed_workspace2"
  organization      = tfe_organization.default.name
  working_directory = "workspace2"
  description       = "Terraform managed workspace"
  tag_names         = ["test", "app", "2"]

  vcs_repo {
    branch             = var.branch
    identifier         = var.identifier
    oauth_token_id     = tfe_oauth_client.default.oauth_token_id
  }
}

resource "tfe_workspace" "tfc" {
  name              = "tf_managed_workspace_tfc"
  organization      = tfe_organization.default.name
  working_directory = "tf_cloud"
  description       = "Terraform managed workspace"
  tag_names         = ["app", "tfc"]
  execution_mode    = "local"

  vcs_repo {
    branch             = var.branch
    identifier         = "dario.clement/terraform-cloud"
    oauth_token_id     = tfe_oauth_client.default.oauth_token_id
  }
}

