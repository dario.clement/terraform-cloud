# Using multiple workspaces:
terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "tf_managed_org"

    workspaces {
      name = "tf_managed_workspace1"
    }
  }
}
